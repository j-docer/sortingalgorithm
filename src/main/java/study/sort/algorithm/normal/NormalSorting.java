package study.sort.algorithm.normal;

public class NormalSorting {

    // 一、冒泡排序：每次最大或最小的值往上面冒泡，直到最后达成有序的数组。
    public static int[] bubble(int[] arrA) {
        if (arrA == null || arrA.length == 0 || arrA.length == 1) {
            return arrA;
        }

        for (int i = 0; i < arrA.length; i++) {
            for (int j = 0; j < arrA.length - 1 - i; j++) {
                System.out.println("arrA[j]:" + arrA[j]);
                if (arrA[j + 1] >= arrA[j]) {
                    int temp = arrA[j + 1];
                    arrA[j + 1] = arrA[j];
                    arrA[j] = temp;
//                    System.out.println(temp);

                }
            }
        }
        return arrA;
    }

    private static void bubbleTest() {
        int[] a = new int[7];
        a[0] = 90;
        a[1] = 23;
        a[2] = 78;
        a[3] = 12;
        a[4] = 1;
        a[5] = 4105;
        a[6] = 545634;
        bubble(a);
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }


    // 二、选择排序:对冒泡排序的优化，跑完一趟之后，没必要两两交换， 可以记下最小值，将最小的值放置最前面即可，这个是最后操作的内容。
    //     因此需要记录最小的值。每次的比较只有一次的交换操作。
    public static void selecttionSort(int[] arr) {
        if (arr == null || arr.length <= 1) {
            return;
        }
        int min_idx;
        for (int i = 0; i < arr.length - 1; i++) {
            min_idx = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min_idx]) {
                    min_idx = j;
                }
            }
            if (min_idx != i) {
                int temp = arr[i];
                arr[i] = arr[min_idx];
                arr[min_idx] = temp;
            }
        }
    }

    private static void selecttionSortTest() {
        int[] arr = new int[]{4, 1, 8, 5, 3, 2, 9, 10, 6, 7, 234, 23, 534, 14, 24, 523, 8451};
        selecttionSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "  ");
        }
    }

    // 三、插入排序：思想： 从后面往前看，遇到合适的位置就插进去，最终数组的序列为从小到大的顺序。
    //     实现思路，默认左边第一个开始就是有序的，右边的是无序的，因此可以从左边到右边对数据进行比较，
    //     遇到该数据的适合插入的位置就可以直接插入。

    // 其他思路：二分查找算法进行优化： 既然左边的数据有序的数据，可以在将右边的数据插入到左边有序数组的时候，能否用二分查找的方法，
    // 不用每次都将遍历一次左边的数组。

    public static void insertSort(int[] arr) {
        if (arr == null || arr.length <= 1) {
            return;
        }
        int temp;
        for (int i = 1; i < arr.length; i++) {
            temp = arr[i]; //[0-i]假设已经是排好序的。
            int leftIndex = i - 1;
            while (leftIndex >= 0 && arr[leftIndex] > temp) {
                arr[leftIndex + 1] = arr[leftIndex];
                leftIndex--;
            }
            arr[leftIndex + 1] = temp;
        }
    }

    private static void insertSortTest() {
        int[] arr = new int[]{4, 1, 8, 5, 3, 2, 9, 10, 6, 7, 234, 23, 534, 14, 24, 523, 8451};
        insertSort(arr);
        System.out.println("insertSort finish");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    //    希尔排序：又叫缩小增量排序。
//
    private static void shellSort(int[] arr) {
        int inc = arr.length / 2;
        for (; inc > 0; inc = inc / 2) {
            // 以下的方法和插入排序相同，只是增量为inc
            // 直接插入排序间隔为1

            int i = inc;
            for (; i < arr.length; i++) {
                int temp = arr[i];
                int t = i - inc;
                while (t >= 0 && arr[t] > temp) {
                    arr[t + inc] = arr[t];
                    t = t - inc;
//                    t -= inc;
                }
                System.out.println("temp:" + temp);
                arr[t + inc] = temp;
            }
        }
    }

    private static void shellSortTest() {
        int[] arr = {4, 6, 7, 4, 3, 7, 34, 95, 33, 67, 88, 33};
        shellSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }


    // 堆排序： 构建初始堆;将待排序的构成一个大顶堆或者小顶堆；
    //          堆顶元素和堆尾元素交换，并断开堆尾元素
    //          重新构建堆
    //          重复以上步骤
    public static void headSort(int[] arr) {
        int length = arr.length;
        // 从第一个非叶子节点从下至上，从左至右调整结构。这一个步骤是构建初始堆，循环中叶子节点会退出，只有
        // 非叶子节点才会进行排序，这次循环中，构建出来的父亲结点都会比孩子结点要大。
        //
        for (int i = (length - 1); i >= 0; i--) {
            headAdjust(arr, i, arr.length);
        }

        // 调整堆顶的元素和末尾的元素
        for (int i = arr.length - 1; i >= 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            headAdjust(arr, 0, i);
        }
    }

    public static void headAdjust(int[] arr, int parent, int arrLength) {
        // 父节点：
        int temp = arr[parent];
        // 左孩子结点
        int lChild = 2 * parent + 1;

        while (lChild < arrLength) {
            int rChild = lChild + 1;
            if (rChild < arrLength && arr[lChild] < arr[rChild]) {
                lChild++;
            }
            // 如果父节点的值大于孩子节点的值，则直接跳出循环
            if (temp >= arr[lChild]) {
                break;
            }
            arr[parent] = arr[lChild];
            // 选取孩子结点的左孩子
            parent = lChild;
            lChild = lChild * 2 + 1;
        }
        arr[parent] = temp;
    }

    // 堆排序测试
    public static void headsortTest() {
        int[] arr = {16, 7, 3, 20, 17, 8, 132, 654, 7489, 46, 8, 64, 46, 67, 46, 8, 46, 89741};
        headSort(arr);
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }

    // 归并排序算法：分治思想的实现，将其分开n/2的，依次分下去，直到，左右为1一个值时候可以执行排序。
    //
    public static void mergeSort(int[] arr, int low, int high) {
        int mid = (low + high) / 2;
        if (low < high) {
            mergeSort(arr, low, mid);
            mergeSort(arr, mid + 1, high);
        }
        merge(arr, low, mid, high);
    }

    public static void merge(int[] arr, int low, int mid, int high) {
        // 存放排序过后并且归并后的临时的数组。
        int[] temp = new int[high - low + 1];
        int i = low;
        int j = mid + 1;
        int k = 0;
        // 把较小的数移动到新数组中
        while (i <= mid && j <= high) {
            if (arr[i] <= arr[j]) {
                temp[k++] = arr[i++];
            } else {
                temp[k++] = arr[j++];
            }
        }

        // 如果左边的数组还没放进去的，则将其移动到临时数组
        while (i <= mid) {
            temp[k++] = arr[i++];
        }
        // 如果右边的数组还没放进去的，则将其移动到临时数组
        while (j <= high) {
            temp[k++] = arr[j++];
        }
        // 最后用临时的数组覆盖原来的数组
        for (int m = 0; m < temp.length; m++) {
            // 数组并非从0开始的，因此需要添加位移量
            arr[low + m] = temp[m];
        }
    }

    public static void mergesortTest() {
        int[] arr = new int[]{4, 1, 8, 5, 3, 2, 9, 10, 6, 7, 234, 23, 534, 14, 24, 523, 8451};
        mergeSort(arr, 0, 16);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }


    // 快速排序：递归，分治思想，但是还是需要实际多次堆栈的存储数据保存内容。
    public static void quickSort(int[] arrA, int start, int end) {
        if (arrA != null && arrA.length > 0) {
            int low = start, high = end;
            int target = arrA[start];
            while (low < high) {
                while (low < high) {
                    // 高位的数字比目标小，因此需要交换。先从高位开始往回走。
                    if (arrA[high] < target) {
                        System.out.println("target" + target);
                        int temp = arrA[low];
                        arrA[low] = arrA[high];
                        arrA[high] = temp;
                        break;
                    } else {
                        high--;
                    }
                }
                while (low < high) {
                    if (arrA[low] > target) {
                        System.out.println("target" + target);
                        int temp = arrA[low];
                        arrA[low] = arrA[high];
                        arrA[high] = temp;
                        break;
                    } else {
                        low++;
                    }
                }
            }

            // 最终low>=high的时候,跳出循环了，再从低位开始排序。
            if ((low - 1) > start) {
                quickSort(arrA, start, low - 1);
            }
            // 低位排序完，再到高位排序。
            if (high + 1 < end) {
                quickSort(arrA, high + 1, end);
            }
        }
    }

    public static void quickSortTest() {
        int[] arr = new int[]{4, 1, 8, 5, 3, 2, 9, 10, 6, 7, 234, 23, 534, 14, 24, 523, 8451};
        quickSort(arr, 0, 16);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void main(String[] args) {
//        bubbleTest();
//        selecttionSortTest();
//        insertSortTest();
//        shellSortTest();
//        headsortTest();
        mergesortTest();
//        quickSortTest();

    }

}
